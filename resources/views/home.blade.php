@extends('layouts.app')

@section('content')

    <div class="container">
        <form id="swipe-form" method="post" action="{{url("submit")}}">
            {{ csrf_field() }}
            <div class="image-panel panel panel-default">
                <div class="panel-heading">Welcome!</div>
                <div class="panel-body">
                    <div>{{$intro}}</div>
                    <br>
                    <div class="btn btn-default" id="start-button">Start</div>
                </div>
            </div>
            @foreach($images as $image)
                <div id="image-panel-{{$image->id}}" class="image-panel panel panel-default hidden">
                    <div class="panel-heading">
                        {{$image->filename}}
                    </div>
                    <input name="images[{{$image->id}}][likes]" class="image-likes" type="hidden">
                    <input name="images[{{$image->id}}][answer]" class="image-answer" type="hidden">
                    <img class="image-swipe" src="{{asset("storage/images/".$image->filename)}}">
                    <div class="btn-group btn-group-justified btn-group-lg image-like-buttons">
                        <button class="image-likes-button btn btn-danger" value=0 style="border-top-left-radius:0;min-height:100px">Dislike</button>
                        <button class="image-likes-button btn btn-success" value=1 style="border-top-right-radius:0;min-height:100px">Likes</button>
                    </div>
                    <div class="panel-body hidden image-likes-question">
                        {{$image->likes_question}}
                        <br>
                        <div class="btn-group-vertical btn-group-lg" style="width: 100%;">
                            @foreach($image->likes_answers as $answer)
                                <button style="width:100%" class="image-question-answer btn btn-default" value="{{$answer}}">{{$answer}}</button>
                            @endforeach
                        </div>
                    </div>
                    <div class="panel-body hidden image-dislikes-question">
                        {{$image->dislikes_question}}
                        <br>
                        <div class="btn-group-vertical btn-group-lg" style="width: 100%;">
                            @foreach($image->dislikes_answers as $answer)
                                <button style="width:100%" class="image-question-answer btn btn-default" value="{{$answer}}">{{$answer}}</button>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="image-panel panel panel-default hidden">
                <div class="panel-heading">Thank you!</div>
                <div class="panel-body">
                    <div>{{$conclusion}}</div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>

    <script>
        $(document).ready(function () {
            // bind 'myForm' and provide a simple callback function
            $('#swipe-form').ajaxForm(function () {
            });
        });

        $("#start-button").on("click", function () {
            var parent = $(this).parents(".image-panel");
            parent.next().removeClass("hidden");
            parent.addClass("hidden");
        });
        $(".image-swipe").hammer().bind("swipe", function (ev) {
            var likes = ev.gesture.direction == 4 ? 1 : 0;
            var selector = ev.gesture.direction == 4 ? ".image-likes-question" : ".image-dislikes-question";
            var parent = $(this).parents(".image-panel");
            parent.find(selector).removeClass("hidden");
            parent.find(".image-likes").val(likes);
            parent.find(".image-swipe").addClass("hidden");
            parent.find(".image-like-buttons").addClass("hidden");
        });

        $(document).on("click", ".image-likes-button", function () {
            var likes = this.value;
            var selector = this.value == 1 ? ".image-likes-question" : ".image-dislikes-question";
            var parent = $(this).parents(".image-panel");
            parent.find(selector).removeClass("hidden");
            parent.find(".image-likes").val(likes);
            parent.find(".image-swipe").addClass("hidden");
            parent.find(".image-like-buttons").addClass("hidden");

        });
        $(document).on("click", ".image-question-answer", function () {
            var parent = $(this).parents(".image-panel");
            parent.find(".image-answer").val(this.value);
            parent.addClass("hidden");
            parent.next().removeClass("hidden");
            $('#swipe-form').submit();
        });
    </script>
@endsection
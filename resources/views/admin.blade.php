@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <form id="images-form" method="post" action="{{url("admin")}}">
            {{ csrf_field() }}
            <table class="table">
                <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Filename</th>
                    <th>Order</th>
                    <th>Likes Question</th>
                    <th>Dislikes Question</th>
                </tr>
                @foreach($images as $image)
                    <tr>
                        <td>{{$image->id}}</td>
                        <td>
                            <img class="small-image" style="max-width:300px" src="storage/images/{{$image->filename}}">
                        </td>
                        <td>{{$image->filename}}</td>
                        <td>
                            <input class="form-control" name="images[{{$image->id}}][order]" value="{{$image->order}}" title="Order">
                        </td>
                        <td>
                            <div class="question">
                                <div class="input-group">
                                    <input class="form-control" name="images[{{$image->id}}][likes_question]" value="{{$image->likes_question}}" title="Likes Question">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default add-button-likes" value="{{$image->id}}">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                                @foreach($image->likes_answers as $answer)
                                    <input class="form-control" name="images[{{$image->id}}][likes_answers][]" value="{{$answer}}" title="Likes Answer">
                                @endforeach
                            </div>
                        </td>
                        <td>
                            <div class="question">
                                <div class="input-group">
                                    <input class="form-control" name="images[{{$image->id}}][dislikes_question]" value="{{$image->dislikes_question}}" title="Disikes Question">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default add-button-dislikes" value="{{$image->id}}">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                                @foreach($image->dislikes_answers as $answer)
                                    <input class="form-control" name="images[{{$image->id}}][dislikes_answers][]" value="{{$answer}}" title="Dislikes Answer">
                                @endforeach
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
            <input type="submit" value="Submit" class="btn btn-primary">
        </form>
    </div>
@endsection

@section('script')
    <script>
        $(".add-button-likes").click("likes", addAnswer);
        $(".add-button-dislikes").click("dislikes", addAnswer);

        function addAnswer(event) {
            var likes = event.data;

            function capitalizeFirstLetter(string) {
                //noinspection JSUnresolvedFunction
                return string.charAt(0).toUpperCase() + string.slice(1);
            }

            var parent = $(this).parents(".question");
            var index = likes + "_answers";
            var title = capitalizeFirstLetter(likes) + " Answer";
            var html = '<input class="form-control" name="images[' + this.value + '][' + index + '][]" value="" title="' + title + '" value="">';
            parent.append(html);
            return false;

        }
    </script>
@endsection
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public $fillable = ["filename"];
    public $guarded = [];
    public function getLikesAnswersAttribute($value)
    {
        return explode("\nsplit\n", $value);
    }
    public function getDislikesAnswersAttribute($value)
    {
        return explode("\nsplit\n", $value);
    }
    public function setLikesAnswersAttribute($value)
    {
        $this->attributes['likes_answers'] = implode("\nsplit\n", $value);
    }
    public function setDislikesAnswersAttribute($value)
    {
        $this->attributes['dislikes_answers'] = implode("\nsplit\n", $value);
    }

}


<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Storage;

class AdminController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $images = Storage::files('images');
        foreach ($images as $index => &$image) {
            $image = Image::firstOrCreate(["filename" => preg_replace('/images\/(.*)/', '$1', $image)]);
        }

        return view("admin", ["images" => $images]);

    }

    public function update(Request $request)
    {
        foreach ($request->images as $image_id => $image_info) {
            $image = Image::find($image_id);
            if ($image) {
                $image->order = $image_info["order"];
                $image->likes_question = $image_info["likes_question"];
                $image->likes_answers = collect($image_info["likes_answers"])->filter(function ($value) {
                    return strlen($value) > 0;
                })->toArray();
                $image->dislikes_question = $image_info["dislikes_question"];
                $image->dislikes_answers = collect($image_info["dislikes_answers"])->filter(function ($value) {
                    return strlen($value) > 0;
                })->toArray();
                $image->save();
            }
        }
        $images = Storage::files('images');
        foreach ($images as $index => &$image) {
            $image = Image::firstOrCreate(["filename" => preg_replace('/images\/(.*)/', '$1', $image)]);
        }

        return view("admin", ["images" => $images]);
    }
}

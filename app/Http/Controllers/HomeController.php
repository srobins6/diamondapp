<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use JavaScript;

class HomeController extends Controller
{

    /**
     * Show the swiping screen.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $images = Image::where("order", ">", 0)->get()->sortBy("order")->keyBy("order");
        Javascript::put(["images" => $images]);
        return view("home", ["images" => $images, "intro" => "Welcome to ShineSwipe!", "conclusion" => "Conclusion text"]);

    }

    /**
     * Submit user responses
     *
     * @param Request $request
     */
    public function submit(Request $request)
    {
        auth()->user()->images()->sync(collect($request->images)->filter(function ($value) {
            return $value["likes"] != "";
        })->toArray());
    }

}

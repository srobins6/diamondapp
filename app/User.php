<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function images()
    {
        return $this->belongsToMany('App\Image')->withPivot('likes', 'answer');
    }

    public function dislikes()
    {
        return $this->belongsToMany('App\Image')->wherePivot('likes', 1);
    }

    public function likes()
    {
        return $this->belongsToMany('App\Image')->wherePivot('likes', 0);
    }

}
